package com.demoprojekt.demoProjekt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoProjektApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoProjektApplication.class, args);
	}

}
